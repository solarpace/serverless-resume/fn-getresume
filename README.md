[![pipeline status](https://gitlab.com/solarpace/serverless-resume/fn-getresume/badges/main/pipeline.svg)](https://gitlab.com/solarpace/serverless-resume/fn-getresume/-/commits/main)

# fn-getresume

OCI Function to retrieve resume data from an AJD (Autonomous Json Database)

This function calls the JDB using SODA Urls to retrieve data based on request body parameter.

Example:

```bash
echo '{"collection":"languages"}' | fn invoke app-resume fngetresume | jq .
```

You should get a json array:
```json
[
  {
    "id": "2",
    "level": "fluent",
    "name": "English"
  },
  {
    "id": "1",
    "level": "mother tongue",
    "name": "French"
  }
]
```


## Prepare your OCI & fn environment

I won't go into all the details for this step as there is already some well documentated steps here : [Oracle Functions in a Local Dev Environment:  Set up, creation, and deployment](https://www.oracle.com/webfolder/technetwork/tutorials/infographics/oci_functions_local_quickview/functions_quickview_top/functions_quickview/index.html)

## Create and initialize a AJD (Autonomous Json Database)

As part of the [Always Free Tier](https://docs.oracle.com/en-us/iaas/Content/FreeTier/freetier_topic-Always_Free_Resources.htm), Oracle gives you the possibility to create lots of free resources, on of them being a JDB; again I won't explain the steps to initialize your DB as everything is documented here : [Introducing Oracle Autonomous JSON Database for application developers](https://blogs.oracle.com/jsondb/autonomous-json-database)

Once you have your AJD up and running, create a new user (RESUME) with Web&REST access:

![AJD user definition](./img/ajb-resume-user-creation.png)

Take not of the ORDS URL for this user, which should looks like (`https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/_sdw/`)


## Getting Started

The first step is to git clone this project: https://gitlab.com/solarpace/serverless-resume/fn-getresume.git (`git clone git@gitlab.com:solarpace/serverless-resume/fn-getresume.git`)

### Create collections and import datas (manual steps)

In this next step, we create the required collections for our resume and import our datas:

Create the collections:

```bash
curl -X PUT  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/jobs'
curl -X PUT  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/skills'
curl -X PUT  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/profiles'
curl -X PUT  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/profiledetails'
curl -X PUT  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/languages'
curl -X PUT  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/educations'
curl -X PUT  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/certifications'
```

For the data, you can find sample files in the **setup/soda/data** folder:

* educations.json
* jobs.json
* languages.json
* profiledetails.json
* profiles.json
* skills.json

Update the files and bulk insert the data with the following commands:

```bash
curl -X POST --data-binary @setup/soda/data/educations.json  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/custom-actions/insert/educations'
curl -X POST --data-binary @setup/soda/data/profiledetails.json  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/custom-actions/insert/profiledetails'
curl -X POST --data-binary @setup/soda/data/jobs.json  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/custom-actions/insert/jobs'
curl -X POST --data-binary @setup/soda/data/languages.json  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/custom-actions/insert/languages'
curl -X POST --data-binary @setup/soda/data/profiles.json  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/custom-actions/insert/profiles'
curl -X POST --data-binary @setup/soda/data/skills.json  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/custom-actions/insert/skills'
curl -X POST --data-binary @setup/soda/data/certifications.json  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/custom-actions/insert/certifications'
```

Check your data with the following command (you can remove the *jq* part if you don't have it installed - but you definitely should 😉)

```bash
curl -X GET  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/educations' | jq '.items[] | .value'
```

👋 **Note** : if you need to reset your data, you can delete a collection with the following command:

```bash
curl -X DELETE --data-binary @education2.json  -u 'RESUME:<your-password>' -H "Content-Type: application/json" --location 'https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest/educations'
```
The result should be:

```json
{
  "dates": "July 2019",
  "id": "aaaaa1",
  "link": null,
  "location": null,
  "school": "-",
  "title": "AWS Certified SysOps Administrator – Associate"
}
{
  "dates": "June 2018",
  "id": "aaaaa2",
  "link": null,
  "location": null,
  "school": "-",
  "title": "AWS Certified Solutions Architect – Associate"
}
{
  "dates": "April 2018",
  "id": "aaaaa3",
  "link": null,
  "location": null,
  "school": "-",
  "title": "Oracle Cloud Infrastructure 2018 Architect - Associate"
}
```

### Create collections and import datas (automatic steps)

You can also automate all those tasks with the script **populateDB.sh**.

Populate the json files in *setup/soda/data** with your resume data and then run:

```bash
sh ./populateDB.sh https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/resume/soda/latest <your-username | RESUME> <your-password>
```


### Deploy your function

Then deploy the function to your OCI tenant, inside the application you have created during the setup  (mine is **app-resume**):

```bash
fn -v deploy --app app-resume
```

Before we can start testing it we need to define the required parameters : **ords-base-url**, **db-schema**, **db-user**, **dbpwd-cipher**, **input-bucket**, **processed-bucket** (last 2 are for a later use)

```bash
fn config function app-resume fngetresume ords-base-url "https://<unique-id>-<dbname>.adb.eu-frankfurt-1.oraclecloudapps.com/ords/"
fn config function app-resume fngetresume db-schema "resume"
fn config function app-resume fngetresume db-user "RESUME"
fn config function app-resume fngetresume dbpwd-cipher "<your-password>"
fn config function app-resume fngetresume input-bucket "input-bucket"
fn config function app-resume fngetresume processed-bucket "processed-bucket"
```

You can now test your function:

```bash
echo '{"collection":"languages"}' | fn invoke app-resume fngetresume | jq .
```

You should get a json array:

```json
[
  {
    "id": "2",
    "level": "fluent",
    "name": "English"
  },
  {
    "id": "1",
    "level": "mother tongue",
    "name": "French"
  }
]
```

### CI/CD

I built some deployment automation thanks to Gitlab CI/CD features.

The *.gitlab-ci.yml* file will automatically redeploy the function when you push your changes.

#### CI/CD variables

You first need to define those variables in GitLab CI/CD settings:

![CI/CD Variables](./img/gitlab-cicd-variables.png)

| Variable Name            | Description | Example  |
|--|--|--|
| FN_APP | Application created in OCI     | app-resume   |
| FN_CONFIG_FILE | Fn configuration file (in ~.fn/config.yaml)   | cli-version: 0.6.8<br/>current-context: solarpace   |
| FN_CONTEXT_CONFIG_FILE | Fn Context file for OCI tenant    | api-url: https://functions.eu-frankfurt-1.oci.oraclecloud.com<br/>oracle.compartment-id: ocid1.compartment.oc1..xxxx<br/>oracle.profile: DEFAULT<br/>provider: oracle<br/>registry: fra.ocir.io/frdp1yrgw7ll/resume   |
| FN_OCI_API_KEY | Your OCI User API Private key   | -----BEGIN RSA PRIVATE KEY-----<br/>MIIExxxxx<br/>-----END RSA PRIVATE KEY-----  |
| FN_OCI_CONFIG_FILE | OCI Cli config file  | [DEFAULT]<br/>user=your-user-ocid<br/>fingerprint=your-user-apikey-fingerprint<br/>key_file=.oci/oci_api_key.pem<br/>tenancy=your-tenancy-ocid<br/>region=eu-frankfurt-1 |
| FN_OCI_REGISTRY | OCI Registry (region specific)   | fra.ocir.io |
| FN_OCI_USER | Your OCI User (namespace/oracleidentitycloudservice/username)  | frxxxxx/oracleidentitycloudservice/user-email |

Once done, each time you push your changes, the CI/CD pipeline will redeploy your function:

![CI/CD Pipeline](./img/gitlab-cicd-pipeline.png)


## Thanks

🍻 Many thanks to [Nathan Glover](https://devopstar.com/author/nathan/) who built the fondation for this : see his [aws-amplify-resume](https://github.com/t04glovern/aws-amplify-resume) repository.

That's all 🎉 (for the moment) 😉
