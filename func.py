import io
import json
import oci
import csv
import requests
import logging

from fdk import response


def soda_get(ordsbaseurl, schema, dbuser, dbpwd, collection):
    auth=(dbuser, dbpwd)
    sodaurl = ordsbaseurl + schema + '/soda/latest/'
    orderby='?q={"$orderby" :  { "id" : 2} }'
    collectionurl = sodaurl + collection + orderby
    headers = {'Content-Type': 'application/json'}
    logging.getLogger().info("calling SODA GET request")
    r = requests.get(collectionurl, auth=auth, headers=headers)
    r_json = {}
    rf_json = []
    try:
        r_json = json.loads(r.text)
    except ValueError as e:
        print(r.text, flush=True)
        raise
    for value in r_json['items']:
        rf_json.append(value['value'])
    return rf_json


def handler(ctx, data: io.BytesIO = None):
    name = "World"
    try:
        cfg = ctx.Config()
        input_bucket = cfg["input-bucket"]
        processed_bucket = cfg["processed-bucket"]
        ordsbaseurl = cfg["ords-base-url"]
        schema = cfg["db-schema"]
        dbuser = cfg["db-user"]
        dbpwd = cfg["dbpwd-cipher"]
    except Exception as e:
        print('Missing function parameters: bucket_name, ordsbaseurl, schema, dbuser, dbpwd', flush=True)
        raise
    try:
        body = json.loads(data.getvalue())
        collection = body.get("collection")
    except (Exception, ValueError) as ex:
        logging.getLogger().info('error parsing json payload: ' + str(ex))

    logging.getLogger().info("calling soda_get")
    result = soda_get(ordsbaseurl, schema, dbuser, dbpwd, collection)

    return response.Response(
        ctx,
        response_data=json.dumps(result),
        headers={"Content-Type": "application/json"}
    )
