#!/bin/bash  
  
if [ ! "$1" ]; then
  read -p "ords endpoint url : " ords_endpoint_url  
else
  ords_endpoint_url=$1
fi
if [ ! "$2" ]; then
  read -p "username : " user_var
else
  user_var=$2
fi
if [ ! "$3" ]; then
  read -sp "password : " pass_var  
else
  pass_var=$3
fi
echo ""

echo "#####################################################"
echo "Populating data using ORDS URL $ords_endpoint_url ..."
echo "#####################################################"
echo "Dropping Collections ..."
for collection in profiles profiledetails jobs skills languages educations certifications
do
  echo "--->Deleting $collection"
  curl -X DELETE  -u "$user_var:$pass_var" -H "Content-Type: application/json" --location $ords_endpoint_url/$collection
done

echo ""
echo "Creating Collections ..."
for collection in profiles profiledetails jobs skills languages educations certifications
do
  echo "--->Creating $collection"
  curl -X PUT  -u "$user_var:$pass_var" -H "Content-Type: application/json" --location $ords_endpoint_url/$collection
done
echo ""
echo "Populating Collections ..."
for collection in profiles profiledetails jobs skills languages educations certifications
do
  echo "--->Populating $collection"
  curl -X POST --data-binary @setup/soda/data/$collection.json  -u "$user_var:$pass_var" -H "Content-Type: application/json" --location $ords_endpoint_url/custom-actions/insert/$collection
  echo ""
done

echo "#####################################################"
echo "Database ready                         "
echo "#####################################################"
